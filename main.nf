#!/usr/bin/env nextflow
nextflow.enable.dsl=2
// The above line must be alone on the line! No comment after it!
// This sets the version of Nextflow to DSL 2, it's called.
// Sorta analogous to python3 compared to 2.7 ... sorta

// This is a comment !

params.barcode_kit = ''
params.num_callers = ''
params.chunk_size = ''
params.chunks_per_runner = ''

// Making directories for tmp inspection, output, reports
[ "./tmp", "./output", "./reports" ].each{ file(it).mkdirs() }

// This is the main workflow !!!
workflow {

    fast5_dir = params.fast5_dir
    guppy_config = params.guppy_config
    barcode_kit = params.barcode_kit
    num_callers = params.num_callers
    chunk_size = params.chunk_size
    chunks_per_runner = params.chunks_per_runner

    println("Reading from: "+fast5_dir)
    println("Basecalling with config: "+guppy_config)

    fast5s = Channel.fromPath(fast5_dir+"**.fast5") 

    basecalled = fast5s 
        | combine( Channel.of( 
                    '-c '+guppy_config+
                    (num_callers == '' ? '' : " --num_callers "+num_callers)+
                    (chunk_size == '' ? '' : " --chunk_size "+chunk_size)+
                    (chunks_per_runner == '' ? '' : " --chunks_per_runner "+chunks_per_runner)
                    ) )
        | guppy_basecall
        | groupTuple(by: 0)
        | map{ it[1..-1] }
        // fastqz, summaries, log

    // just take stack of summaries and make NanoPlot QC report
    qc = basecalled | map{ it[1] } | gen_qc_report

    basecalled 
        | filter{ ! barcode_kit }
        | map{ [it[0]] }
        | guppy_basecall_tidy

    demuxed = basecalled 
        | filter{ barcode_kit }
        | map{ [it[0]] }
        | combine( Channel.of( 
                    barcode_kit == '' ? '' : "--barcode_kits "+barcode_kit
                    ) )
        | guppy_barcoder
        | guppy_barcoder_tidy

}

process gen_qc_report {
    publishDir 'output', mode: 'rellink', overwrite: true
    label 'nanoplot'
    label 'all_core'
    label 'smol_mem'
    time '12h'
    input:  path("sequencing_summary_??????.txt")
    output: path("nanoplot_qc",type:'dir')
    shell: '''
head -n1 sequencing_summary_000001.txt > sequencing_summary.txt
tail -qn+2 sequencing_summary_*.txt >> sequencing_summary.txt
mkdir nanoplot_qc
cd nanoplot_qc && \
    NanoPlot -t !{task.cpus} \
        --summary ../sequencing_summary.txt 
'''
}




// guppy basecalling
process guppy_basecall {
    publishDir 'tmp'
    label 'all_core'
    label 'all_mem'
    time '36h'
    //label 'guppy_gpu'
    input: tuple path(fast5), val(guppy_params)
    output: tuple val(guppy_params), 
        path("basecalled/*fastq.gz"), 
        path("basecalled/sequencing_summary.txt"),
        path("basecalled/*log")
    shell: '''
guppy_basecaller \
    --device 'cuda:0' \
    --disable_pings \
    --compress_fastq --records_per_fastq 0 \
    --disable_qscore_filtering \
    --recursive -s basecalled \
    -i ./ \
    !{guppy_params}
#    --do_read_splitting \
'''
}

// guppy basecalling
process tidy_up_guppy {
    publishDir 'tmp'
    label 'all_core'
    label 'all_mem'
    time '72h'
    label 'bioinfmunger'
    input: tuple val(run_id), val(library), path(basecalled)
    output: tuple val(run_id), val(library), path("basecalled",type:'dir'),
        path("guppy.fastq.gz")
        //path("guppy_{pass,fail}.fastq.gz"), 
    shell: '''
cat basecalled/*fastq.gz > guppy.fastq.gz
'''
}

// guppy demux
process guppy_barcoder {
    label 'all_core'
    label 'all_mem'
    time '36h'
    //label 'guppy_gpu'
    input: tuple path("fastqs/??????.fastq.gz"), val(barcode_kit_string)
    output: path("demuxed",type:'dir')
    shell: '''
mkdir demuxed
guppy_barcoder \
    --input_path ./ \
    --recursive \
    --save_path demuxed \
    --compress_fastq \
    --records_per_fastq 0 \
    --disable_pings \
    --device 'cuda:0' \
    !{barcode_kit_string} 
'''
}

// guppy demux
process guppy_barcoder_tidy {
    publishDir 'output', mode: 'rellink', overwrite: true
    label 'all_core'
    label 'all_mem'
    label 'bioinfmunger'
    input: path("demuxed")
    output: path("demuxed.fastq.gz")
    shell: '''
find demuxed/ -regex ".*fastq.gz" \
    | xargs zcat -f \
    | paste - - - - \
    | sed 's/ [^\t]*barcode=/_/' \
    | awk '{print $1"\\n"$2"\\n"$3"\\n"$4}' \\
    | gzip -c \
    > demuxed.fastq.gz
'''
}


// guppy demux
process guppy_basecall_tidy {
    publishDir 'output', mode: 'rellink', overwrite: true
    label 'all_core'
    label 'all_mem'
    time '36h'
    label 'bioinfmunger'
    input: tuple path("fastqs/??????.fastq.gz")
    output: path("all_reads.fastq.gz")
    shell: '''
cat fastqs/*gz > all_reads.fastq.gz
'''
}


