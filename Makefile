# Phony rules tells make to just ignore these fake files
.PHONY: all basecall clean lightclean

# This makes the nextflow executable for running
nextflow: 
	curl -s https://get.nextflow.io | bash

all: basecall

# this is just for testing....
basecall: main.nf nextflow.config nextflow
	./nextflow $(word 1,$^) -c $(word 2,$^) \
		-resume -ansi-log false -with-dag reports/dag.html \
		-profile common,bubbly \
		--fast5_dir /home/l/seq_data/ppiseq/20220330_1047_MN21180_FAS93040_f1f3734f/fast5 \
		--guppy_config dna_r10.4_e8.1_fast.cfg \
		--sample_sheet samplesheet.tsv \
		--barcode_kit SQK-NBD112-24 \
		--num_callers 4 --chunk_size 2000 --chunks_per_runner 32
		
# --num_callers 4 --chunk_size 2000 --chunks_per_runner 32

#  multiplexing: |
#      barcode17   lib4
#          barcode18   lib5
#              barcode19   lib6
#                kit: 'SQK-NBD112-24'
#                  guppy-barcoder-args: '--barcode_kits SQK-NBD112-24'
#                    filter-out: 
#
