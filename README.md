# Nanopore basecalling and demultiplexing pipeline

This aims to be a command-line API nextflow pipeline for doing demultiplexing
from nanopore data. It requires that the system has `nextflow` installed in
a users `$PATH`, and assumes you're on `blocky`.

Talk to Darach about details.

# To use

1. Log in to `blocky`. 
1. Get the nanopore folder in the right place. Ask Darach. 
1. Navigate inside the nanopore run folder. You should still be on the main drive, 
    so probably the folder made by Nanopore in `/var/lib/minknow/data` or something like that.
    Go inside the folder with the name, so something like `Project/ProjectName/20221202_asdaljkhclkahflakshflakh`
    or the like, the run folder is the one that starts with the date.
1. Run `sudo chgrp -R stitch /var/lib/minknow/data` . This makes it possible to modify the directory.
1. Make a directory called `basecalling`, so you'll see the `fast5` and `basecalling`
    directories in the same directory, right next to each other. 
    DO NOT USE `sudo mkdir`, or you'll have to change the group again.
    Go inside the `basecalling` directory. 
1. From inside the `basecalling` directory inside the nanopore run folder,
    launch the pipeline with something like:
    
         nextflow run http://gitlab.com/darachm/guppy-basecaller -r v1.0.4 \
            -resume -ansi-log false -with-dag reports/dag.html \
            -profile common,blocky \
            --fast5_dir ../fast5 \
            --guppy_config dna_r10.4.1_e8.2_260bps_hac.cfg \
            --chunks_per_runner 235 \
            --barcode_kit SQK-RBK114-96             # CHECK THAT ITS THE 96 kit

1. Watch it launch, make sure it's running a few minutes and you have enough
    disk space for results.
1. It currently takes 2 min per fast5 file. Come back later.
1. Your files will be relative symlinkd[^symlink] from inside the `output` 
    directory to the final files in the `work` directory. You can copy
    the files somewhere else, or just point towards them.
    Pipeline metrics (time, RAM, CPU) will be summarized in the `report`
    directory.

Current practice is to then rsync this entire nanopore run folder back up
to the backups, so the nanopore runs will have original folder structure,
plus a `basecalling` folder with the basecalling run, metadata, output,
and QC files.

`output` contains:
  - Final `*.fastq.gz` file that has just the basecalled data or the 
      basecalled and demuxed data (with appropriate barcodeID in the header).
  - QC report from NanoPlot, see the HTML in there for report


[^symlink]: Symbolic links are a file that points to another file. In this case,
that means the output folder files don't have the actual data, but they point
to a systematically named file inside the `work` folder 
(like `work/ad/1ad8048e0a.../all_reads.fastq.gz` etc) that is the actual file.
If you access the file in the output, you get the data. If you copy that
file down using rsync or whatever, you should get your data.
Just a techincal note so there's no confusion.

# Notes

- Omit `barcode_kit` and there's no demultiplexing by barcode, if there is
    than guppy barcoder will restrict to using the specified kit. Make
    sure it's right! See guppy documentation (have to login to nanopore...)
    for more details of behaviour, this is just handed to guppy.
- `chunks_per_runner` is a tuning parameter for guppy to run, this is a bit
    senstitive to tune. 158 is tuned for the GTX 1080ti in 'bubbly',
    235 is tuned for the RTX 3080ti in 'blocky'.
- If you did basecalling during the run by accident, then I don't know how to 
    fix that. Maybe copy them all into one directory? Use symlinks?
- It runs demultiplexing for each `fast5` file. This design choice is so that
    we can run this for runs for which we have partial data collected, so while
    it's sequencing. I wonder if it's slower, but I don't know. 
- It has to rerun each time you change the guppy parameter, so not great 
    for tuning to the hardware. There's another pipeline for that, ask Darach.

# Todo

- Any way to run with different guppy performance settings without
    having to restart?
- Spin out `guppy_basecaller` as server/client config, maybe have htat
    also be a mode option via cli? 
- Move to live basecalling as part of running the nanopore!!!
